from matplotlib.patches import FancyArrowPatch, ConnectionStyle
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d
from .transform import *


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


def plotFrame3D(ax, origin, x, y, z, name="", origin_labels=True, axis_labels=True,
                mutation_scale=5, arrowstyle='->', fontsize=7, **kwargs):
    colors = ['r', 'b', 'g']
    directions = [r'$X$', r'$Y$', r'$Z$']
    vectors_len_tot = np.zeros(3)
    for n, d in enumerate([x, y, z]):
        vectors = [[origin[c], d[c]] for c in range(len(d))]
        a = Arrow3D(vectors[0], vectors[1], vectors[2], color=colors[n],
                    mutation_scale=mutation_scale, arrowstyle=arrowstyle, shrinkA=0, shrinkB=0,
                    **kwargs)
        ax.add_artist(a)
        vectors_len = np.array([v[1] - v[0] for v in vectors])
        # place text 10 % after end of axis arrow
        vectors_len_tot += vectors_len
        if axis_labels:
            ax.text(vectors[0][1] + vectors_len[0] * 0.1,
                    vectors[1][1] + vectors_len[1] * 0.1,
                    vectors[2][1] + vectors_len[2] * 0.1,
                    directions[n] + name, fontsize=fontsize)

    # place behind z-axis by default
    if origin_labels:
        ax.text(origin[0] - vectors_len_tot[0] * 0.1,
                origin[1] - vectors_len_tot[1] * 0.1,
                origin[2] - vectors_len_tot[2] * 0.1,
                r'$o$' + name, fontsize=fontsize)


def plotCoord3D(ax, H, sz=1, name="", **kwargs):
    plotFrame3D(ax, trans(H, [0, 0, 0]),
                trans(H, [sz, 0, 0]),
                trans(H, [0, sz, 0]),
                trans(H, [0, 0, sz]), name, **kwargs)


def plotArrow3D(ax, start, end, label=None, label_offset=[0, 0, .1], angle=0,
                mutation_scale=5, arrowstyle='->', fontsize=7, **kwargs):
    r_vect = np.c_[start, end]
    consStyle = ConnectionStyle("Arc3", rad=angle)
    a = Arrow3D(r_vect[0, :], r_vect[1, :], r_vect[2, :],
                shrinkA=0, shrinkB=0, connectionstyle=consStyle,
                mutation_scale=mutation_scale, arrowstyle=arrowstyle, **kwargs)
    ax.add_artist(a)
    if label is not None:
        arrow_text_pos = r_vect[:, 0] + (r_vect[:, 1] - r_vect[:, 0]) / 2 + np.array(label_offset)
        print(f"Vector: {r_vect}, arrow_text_pos: {arrow_text_pos}")
        ax.text(arrow_text_pos[0], arrow_text_pos[1], arrow_text_pos[2],
                label, fontsize=fontsize)


def plotArc3d(ax, vec1, vec2, dist=0.2, label_offset=[0, 0, 0], fontsize=7,
              arrowstyle='--', angle=-np.pi / 10, linewidth=0.6, **kwargs):
    vec_sum = (vec1 + vec2) / 2
    label_offset += 0.05 * vec_sum / np.linalg.norm(vec_sum) - np.array([0, 0, 0.005 * fontsize])
    plotArrow3D(ax, vec1 * dist, vec2 * dist, arrowstyle=arrowstyle, label_offset=label_offset,
                linewidth=linewidth, angle=angle, **kwargs)
    # # label position
    # if label is not None:
    #     label_pos = label_offset * dist * (vec1 + vec2) / 2
    #     ax.text(label_pos[0], label_pos[1], label_pos[2],
    #             label, fontsize=fontsize)


if __name__ == "__main__":
    # demonstration
    fig = plt.figure(tight_layout=True, figsize=[7, 5], dpi=100)
    ax = fig.add_subplot(111, projection='3d')
    # # ax.set_axis_off()
    ax.set_xlim3d(0, 4)
    ax.set_ylim3d(-2, 2)
    ax.set_zlim3d(-2, 2)

    # plot two coordinate systems and an arrow between them
    H_wf = H_matrix(R_x(np.pi), [3, 0, 1])
    plotCoord3D(ax, H_matrix(), r"$_{world}$")
    plotCoord3D(ax, H_wf, r"$_{target}$")
    plotArrow3D(ax, [0, 0, 0], trans(H_wf, [0, 0, 0]))

    plt.show()
