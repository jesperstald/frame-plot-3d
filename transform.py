import numpy as np


def R_x(th):
    matr = np.array([[1, 0, 0],
                     [0, np.cos(th), -np.sin(th)],
                     [0, np.sin(th), np.cos(th)]])
    return matr


def R_y(th):
    matr = np.array([[np.cos(th), 0, np.sin(th)],
                     [0, 1, 0],
                     [-np.sin(th), 0, np.cos(th)]])
    return matr


def R_z(th):
    matr = np.array([[np.cos(th), np.sin(th), 0],
                     [-np.sin(th), np.cos(th), 0],
                     [0, 0, 1]])
    return matr


def H_matrix(rot=None, dis=None):
    # return homogeneous matrix
    if rot is None:
        rot = np.eye(3)
    if dis is None:
        dis = np.array([[0], [0], [0]])

    h = np.r_[np.c_[rot, dis], np.array([[0, 0, 0, 1]])]
    return h


def H_inv(h_in):
    h_out = np.eye(4)
    R_t = h_in[0:3, 0:3].T
    h_out[0:3, 0:3] = R_t
    h_out[0:3, -1] = -R_t @ h_in[0:3, -1]

    return h_out


def trans(H, p):
    p_out = H @ np.r_[p, 1]
    return p_out[:3]
